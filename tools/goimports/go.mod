module gitlab.com/gitlab-org/gitaly/tools/goimports

go 1.21

require golang.org/x/tools v0.23.0

require (
	golang.org/x/mod v0.19.0 // indirect
	golang.org/x/sync v0.7.0 // indirect
)
